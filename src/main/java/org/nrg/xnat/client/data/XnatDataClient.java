/*
 * XnatDataClient
 * (C) 2016 Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD License
 */
package org.nrg.xnat.client.data;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.io.Files;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.*;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.AuthCache;
import org.apache.http.client.CookieStore;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.conn.socket.LayeredConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.entity.InputStreamEntity;
import org.apache.http.entity.StringEntity;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.cookie.Cookie;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.FileEntity;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.*;
import org.apache.http.impl.cookie.BasicClientCookie;
import org.apache.http.util.EntityUtils;
import org.nrg.framework.net.HttpMethod;
import org.nrg.framework.pinto.PintoApplication;
import org.nrg.framework.pinto.PintoException;
import org.nrg.framework.pinto.PintoExceptionType;
import org.nrg.framework.utilities.Reflection;

import javax.activation.MimetypesFileTypeMap;
import java.io.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.*;

@PintoApplication(value = "XDC: the XNAT Data Client", version = "property:version", copyright = "(C)2016 Washington University School of Medicine", introduction = "The XNAT Data Client supports data transfer operations to and from the XNAT server. Check for the latest version of the XNAT Data Client at ftp://ftp.nrg.wustl.edu/pub/xnat/xnat-data-client-latest.jar.")
public class XnatDataClient {

    public static void main(String[] args) throws Exception {
        if (_log.isDebugEnabled()) {
            final boolean hasArgs = args != null && args.length > 0;
            final StringBuilder message = new StringBuilder(hasArgs ? "Starting instance of XDC with the parameters: " : "Starting instance of XDC with no parameters");
            boolean isFirst = true;
            if (hasArgs) {
                for (String arg : args) {
                    if (!isFirst) {
                        message.append(", ");
                    } else {
                        isFirst = false;
                    }
                    message.append(arg);
                }
            }
            _log.debug(message);
        }
        XnatDataClient xdc = null;
        try {
            xdc = new XnatDataClient(args);
            if (xdc._bean.getShouldContinue()) {
                int status = xdc.launch();
                System.exit(status);
            }
        } catch (PintoException exception) {
            // Only warn on non-syntax format errors
            if (exception.getType() != PintoExceptionType.SyntaxFormat) {
                _log.warn(exception);
            }
            if (xdc != null) {
                xdc._bean.displayHelp();
            }
        }
    }

    public XnatDataClient(final String args) throws PintoException {
        this(args.split("\\s+"));
    }

    public XnatDataClient(final String[] args) throws PintoException {
        _properties = Reflection.getPropertiesForClass(getClass());
        if (_properties == null || _properties.stringPropertyNames().size() == 0) {
            throw new PintoException(PintoExceptionType.Configuration, "Couldn't load properties bundle for class: " + getClass().getName());
        }
        _bean = new XnatDataClientPintoBean(this, checkForOsaOverride(args));
        _printStream = _bean.getPrintStream();

        _clientBuilder = HttpClientBuilder.create();
        _context = HttpClientContext.create();
        _cookieStore = new BasicCookieStore();
    }

    public int launch() throws Exception {
        _log.info("Launching XnatDataClient...");

        if (_bean.getRemote() == null) {
            throw new Exception("You must specify a remote URL until XDC supports file-system mapping.");
        }

        final HttpRequestBase request = HttpMethod.getHttpRequestObject(_bean.getMethod());
        request.setURI(buildUri());

        if (_bean.getLocal() != null) {
            setEntity(request);
        }

        final RequestConfig.Builder builder = RequestConfig.custom();
        final HttpHost proxy = configureProxy();
        if (proxy != null) {
            builder.setProxy(proxy);
        }
        request.setConfig(builder.build());
        configureAuthentication();

        if (_bean.hasHeaders()) {
            for (final String header : _bean.getHeaders().keySet()) {
                request.setHeader(header, _bean.getHeaders().get(header));
            }
        }

        _clientBuilder.setDefaultCookieStore(_cookieStore);
        if (_bean.getAcceptSelfSigned()) {
            _clientBuilder.setSSLSocketFactory(getPermissiveSocketFactory(true));
        } else if (_bean.getIgnoreCertErrors()) {
            _clientBuilder.setSSLSocketFactory(getPermissiveSocketFactory(false));
        }
        _client = _clientBuilder.build();

        try (final InputStream stream = getDataStream()) {
            final boolean hasData = _bean.getData() != null;
            if (stream != null) {
                ((HttpEntityEnclosingRequest) request).setEntity(new InputStreamEntity(stream));
            } else if (hasData && !isPlainText()){
                ((HttpEntityEnclosingRequest) request).setEntity(new UrlEncodedFormEntity(URLEncodedUtils.parse(_bean.getData(), StandardCharsets.UTF_8)));
            }
            try (CloseableHttpResponse response = _context == null ? _client.execute(request) : _client.execute(request, _context)){
                handleEntity(response);
                int status = response.getStatusLine().getStatusCode();

                if (_log.isInfoEnabled()) {
                    _log.info("Status: " + response.getStatusLine());
                }
                if (_bean.getShowStatusLine()) {
                    _printStream.println("HTTP status: " + response.getStatusLine());
                } else if (_bean.getShowStatusCode()) {
                    _printStream.println("HTTP status: " + status);
                }

                return status < 400 ? 0 : status;
            }

        } finally {
            if (_client != null) {
                _client.close();
            }
        }
    }

    private InputStream getDataStream() throws Exception {
        final boolean hasData = _bean.getData() != null;
        final boolean hasBinaryData = _bean.getDataBinary() != null;
        if (!hasData && !hasBinaryData) {
            return null;
        }
        final boolean isPlainText = isPlainText();
        if (isPlainText && hasBinaryData) {
            throw new Exception("You've specified content type text/plain but are passing binary data.");
        }
        if (isPlainText || hasBinaryData) {
            final String data = isPlainText ? _bean.getData() : _bean.getDataBinary();
            if (data.startsWith("@")) {
                final File file = Paths.get(_bean.getData().substring(1)).toFile();
                if (!(file.exists() && file.isFile())) {
                    throw new Exception("The specified file " + file.getPath() + " does not exist or is not accessible.");
                }
                return Files.asByteSource(file).openBufferedStream();
            } else {
                return new ByteArrayInputStream(data.getBytes(StandardCharsets.UTF_8));
            }
        }
        return null;
    }

    private boolean isPlainText() {
        return _bean.hasHeaders() && _bean.getHeaders().containsKey("Content-Type") && _bean.getHeaders().get("Content-Type").equals("text/plain");
    }

    private LayeredConnectionSocketFactory getPermissiveSocketFactory(final boolean selfSignedCompatible) throws KeyStoreException, NoSuchAlgorithmException, KeyManagementException {
        SSLContextBuilder builder = new SSLContextBuilder();
        builder.loadTrustMaterial(null, selfSignedCompatible ? new TrustSelfSignedStrategy() : new AllowAllCertsStrategy());
        return new SSLConnectionSocketFactory(builder.build());
    }

    private URI buildUri() throws URISyntaxException {
        final URIBuilder builder = new URIBuilder(_bean.getRemote().toString());

        if (_bean.getUseAbsolutePath()) {
            builder.addParameter("locator", "absolutePath");
        }
        if (_bean.getData() != null) {
            builder.addParameters(URLEncodedUtils.parse(_bean.getData(), StandardCharsets.US_ASCII));
        }
        if (_bean.getDataBinary() != null) {
            builder.addParameters(URLEncodedUtils.parse(_bean.getDataBinary(), Charset.defaultCharset()));
        }
        return builder.build();
    }

    private void setEntity(HttpRequestBase request) {
        File local = _bean.getLocal();
        if (local != null) {
            if (request instanceof HttpEntityEnclosingRequest) {
                FileEntity entity = new FileEntity(local, ContentType.create(getMimeType(local)));
                ((HttpEntityEnclosingRequest) request).setEntity(entity);
            }
        }
    }

    protected Properties getProperties() {
        return _properties;
    }

    protected PrintStream getPrintStream() {
        return _printStream;
    }

    private String[] checkForOsaOverride(final String[] args) {
        // If the args don't contain an display stream adapter override...
        if (!(ArrayUtils.contains(args, "-osa") || ArrayUtils.contains(args, "--outputStreamAdapter"))) {
            // Check to see if one is configured in the application properties.
            if (getProperties().contains("output.stream.adapter")) {
                return (String[]) ArrayUtils.add(ArrayUtils.add(args, "-osa"), getProperties().get("output.stream.adapter"));
            }
        }
        return args;
    }

    private String getMimeType(File file) {
        return MIME_TYPE_MAP.getContentType(file);
    }

    private void handleEntity(final HttpResponse response) throws IOException, URISyntaxException {
        HttpEntity entity = response.getEntity();

        if (entity == null) {
            _log.debug("I tried to handle the response entity, but the entity was null.");
            return;
        }

        try {
            long size = entity.getContentLength();
            if (size == 0) {
                _log.debug("Entity returned, but size was 0, so just finishing the entity handling.");
                return;
            }

            final Header contentType = entity.getContentType();
            final String value = contentType != null ? contentType.getValue() : "";
            if (!StringUtils.isBlank(value) && value.equalsIgnoreCase("multipart/form-data")) {
                throw new RuntimeException("XDC does not currently handle multipart form data.");
            }

            if (_log.isDebugEnabled()) {
                _log.debug(String.format("Found an entity: %s %d bytes in length", StringUtils.isBlank(value) ? "No content type found" : value, entity.getContentLength()));
            }

            // If this is a batch processing run...
            if (_bean.getBatch()) {
                if (!"application/json".equalsIgnoreCase(value)) {
                    throw new RuntimeException("XDC can currently only perform batch transfers with JSON inputs.");
                }
                handleBatchTransfer(entity);
            } else {
                handleSingleTransfer(entity);
            }
        } finally {
            EntityUtils.consume(entity);
        }
    }

    /**
     * This takes an entity that contains JSON code and pulls URIs (if {@link XnatDataClientPintoBean#getUseAbsolutePath()}
     * is set to <b>false</b>) and absolute paths (if {@link XnatDataClientPintoBean#getUseAbsolutePath()} is set to
     * <b>true</b>) and downloads or copies the resulting files.
     *
     * @param entity The entity returned by a response to a REST call that contains JSON with URIs or paths.
     */
    private void handleBatchTransfer(final HttpEntity entity) throws IOException, URISyntaxException {
        String targetColumn = _bean.getUseAbsolutePath() ? "absolutePath" : "URI";
        Map<String, String> items = new HashMap<>();
        ObjectMapper mapper = new ObjectMapper();
        try {
            final Iterator<JsonNode> nodes = mapper.readTree(entity.getContent()).path("ResultSet").path("Result").elements();
            while (nodes.hasNext()) {
                final JsonNode node = nodes.next();
                String name = node.get("Name").asText();
                String target = node.get(targetColumn).asText();
                items.put(name, target);
                addPath(name, target);
            }
        } finally {
            EntityUtils.consume(entity);
        }

        final String commonPath = getCommonPath();
        for (Map.Entry<String, String> item : items.entrySet()) {
            if (_bean.getListUris()) {
                _printStream.println(item.getValue());
            } else {
                // Here the name is the name of the resource file, the path is the full path to the item, and partial is
                // the difference between the common path and the full path to the item. This is so that paths within
                // archive structures can be constructed at the destination.
                String name = item.getKey();
                String path = item.getValue();
                String partial = StringUtils.reverse(StringUtils.difference(StringUtils.reverse(name), StringUtils.reverse(StringUtils.difference(commonPath, path))));
                transfer(name, path, partial);
            }
        }
    }

    private String getCommonPath() {
        return _commonPath; // != null ? new String(_commonPath) : "";
    }

    private void addPath(final String name, final String fullPath) {
        final String path = StringUtils.removeEnd(fullPath, name);
        if (_commonPath == null) {
            _commonPath = path;
        } else {
            trimPath(path);
        }
    }

    private void trimPath(final String path) {
        if (path.equals(_commonPath)) {
            return;
        }
        int pathLength = path.length();
        int commonPathLength = _commonPath.length();
        if (pathLength > commonPathLength) {
            if (path.startsWith(_commonPath)) {
                return;
            }
        } else if (commonPathLength > pathLength) {
            if (_commonPath.startsWith(path)) {
                _commonPath = path;
                return;
            }
        }
        char[] shorter = pathLength < commonPathLength ? path.toCharArray() : _commonPath.toCharArray();
        char[] longer = pathLength >= commonPathLength ? path.toCharArray() : _commonPath.toCharArray();
        int index = 0;
        int lastSeparator = 0;
        while (shorter[index] == longer[index]) {
            if (shorter[index] == '/') {
                lastSeparator = index;
            }
            index++;
        }
        _commonPath = new String(Arrays.copyOf(shorter, lastSeparator + 1));
    }

    private void transfer(final String name, final String target, final String partial) throws URISyntaxException, IOException {
        HttpEntity instance = null;
        InputStream input = null;
        OutputStream output = null;
        try {
            File incoming = null, outgoing;
            if (_bean.getUseAbsolutePath()) {
                incoming = new File(_bean.hasPathPrefix() ? target.replace(_bean.getPathPrefixSource(), _bean.getPathPrefixDestination()) : target);
                input = new FileInputStream(incoming);
            } else {
                URI uri = new URI(_bean.getRemote().getScheme(), _bean.getRemote().getUserInfo(), _bean.getRemote().getHost(), _bean.getRemote().getPort(), target, null, null);
                HttpGet request = new HttpGet(uri);
                HttpResponse response = _context == null ? _client.execute(request) : _client.execute(request, _context);
                instance = response.getEntity();
                input = instance.getContent();
            }

            File full = getRelativePathToOutput(partial);
            if (!full.exists()) {
                if (!full.mkdirs()) {
                    throw new RuntimeException("Failed to create the directory: " + full.getAbsolutePath() + ". Please check permissions and accessibility to this path.");
                }
            }
            outgoing = new File(full, name);
            if (_log.isDebugEnabled()) {
                _log.debug("Starting the transfer for " + name + " to partial path " + partial + " from source " + target);
            }
            if (_bean.getUseSymlinks()) {
                assert incoming != null;
                doSymlink(incoming, outgoing);
                display("Created symlink from " + incoming.getPath() + " to " + outgoing.getPath());
            } else {
                output = new FileOutputStream(outgoing);
                long totalBytesWritten = doTransfer(input, output);
                notifyOnOutput(outgoing, totalBytesWritten);
            }
        } finally {
            if (instance != null) {
                EntityUtils.consume(instance);
            }
            if (input != null) {
                input.close();
            }
            if (output != null) {
                output.flush();
                output.close();
            }
        }
    }

    private File getRelativePathToOutput(final String partial) {
        File output;
        if (!StringUtils.isBlank(_bean.getOutputName())) {
            output = new File(_bean.getOutputName());
            if (output.exists()) {
                if (!output.isDirectory()) {
                    throw new RuntimeException("You must specify your output to be a directory to use this function.");
                }
            } else {
                boolean made = output.mkdirs();
                if (_log.isDebugEnabled()) {
                    _log.debug(made ? "Created" : "Found" + " output folder: " + output.getAbsolutePath());
                }
            }
        } else {
            output = new File(".");
            if (_log.isDebugEnabled()) {
                _log.debug("Using working directory for output folder: " + output.getAbsolutePath());
            }
        }

        try {
            return new File(output.getCanonicalFile(), partial);
        } catch (IOException e) {
            throw new RuntimeException("Encountered error trying to resolve canonical file to: " + output.getAbsolutePath(), e);
        }
    }

    /**
     * Super simple abstraction around _printStream.println to allow for easy migration to more flexible output method
     * later. All display messages are also written out as debug logging messages.
     *
     * @param output The text to be displayed.
     */
    private void display(String output) {
        _printStream.println(output);
        if (_log.isDebugEnabled()) {
            _log.debug(output);
        }
    }

    private void doSymlink(final File incoming, final File outgoing) {
        try {
            Runtime.getRuntime().exec("ln -s " + incoming.getAbsolutePath() + " " + outgoing.getAbsolutePath());
        } catch (IOException exception) {
            _log.error("Error trying to create symlinks", exception);
        }
    }

    private void handleSingleTransfer(final HttpEntity entity) throws IOException {
        if (entity == null) {
            throw new RuntimeException("There is no HTTP entity associated with the attempted transfer. Please check your URL and permissions at the server.");
        }

        // Do the single transfer operation.
        // If we have a specified output name, we'll use that regardless of whether this is text or not.

        // Figure out the appropriate display location.
        OutputStream output = null;
        InputStream input = null;
        try {
            final File local;
            final String contentType = entity.getContentType() != null ? entity.getContentType().getValue() : "";
            if (!StringUtils.isBlank(_bean.getOutputName())) {
                local = new File(_bean.getOutputName());
                output = new FileOutputStream(resolveTarget(local, getSuggestedExtension(contentType)));
            } else if (!isTextContentType(contentType)) {
                // TODO: It'd be nice to re-use the downloaded file name if possible.
                local = generateUniqueFile(getSuggestedExtension(contentType));
                output = new FileOutputStream(local);
            } else {
                // This is a text file with no local, so we'll write out to the print stream.
                local = null;
                output = getPrintStream();
            }

            input = entity.getContent();
            long totalBytesWritten = doTransfer(input, output);
            notifyOnOutput(local, totalBytesWritten);
        } finally {
            if (output != null) {
                output.flush();
                // Only close this if it's a file, otherwise it's standard out.
                if (output instanceof FileOutputStream) {
                    output.close();
                }
            }
            if (input != null) {
                input.close();
            }
        }
    }

    private void notifyOnOutput(final File local, final long totalBytesWritten) {
        if (totalBytesWritten > 0) {
            display("\nWrote " + totalBytesWritten + " bytes to " + (local != null ? local.getPath() : "output stream"));
        } else {
            if (local != null) {
                boolean delete = local.delete();
                if (!delete) {
                    display("No contents found in response body, unable to delete indicated output file: " + local.getPath());
                } else {
                    display("No contents found in response body, no output file generated");
                }
            } else {
                display("No contents found in response body, no output file generated");
            }
        }
    }

    private File resolveTarget(final File local, final String suggestedExtension) {
        final File target;
        if (local.exists() && local.isDirectory()) {
            // TODO: It'd be nice to re-use the downloaded file name if possible.
            target = generateUniqueFile(local, suggestedExtension);
        } else if (!hasExtension(local)) {
            target = new File(local.getAbsolutePath() + "." + suggestedExtension);
        } else {
            target = local;
            if (!_bean.getOverwrite() && target.exists()) {
                getPrintStream().print("The file " + target.getName() + " already exists. Do you want to overwrite it? ");
                BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
                String confirm = null;
                try {
                    confirm = reader.readLine();
                } catch (IOException ioe) {
                    System.err.println("IO error reading input.");
                    System.exit(-1);
                }
                if (confirm == null || !(confirm.equalsIgnoreCase("y") || confirm.equalsIgnoreCase("yes"))) {
                    getPrintStream().println("Operation aborted.");
                    System.exit(0);
                }
            }
        }
        return target;
    }

    /**
     * This tells you whether the file has an extension that is separated by the '.' character and consists of one or
     * more alphanumeric characters.
     *
     * @param file The file with the name to be tested.
     *
     * @return True if the file name has an extension, false otherwise.
     */
    private boolean hasExtension(final File file) {
        final String name = file.getName();
        if (!name.contains(".")) {
            return false;
        }
        final String lastElement = name.substring(name.lastIndexOf("."));
        return !StringUtils.isBlank(lastElement) && lastElement.matches("^\\.[A-z0-9]+");
    }

    /**
     * Transfers content from the content input stream to the output output stream.
     *
     * @param content The stream from which to read.
     * @param output  The stream to which to write.
     *
     * @return The total number of bytes written during the transfer.
     *
     * @throws IOException
     */
    private long doTransfer(final InputStream content, final OutputStream output) throws IOException {
        if (content == null) {
            throw new RuntimeException("Tried to retrieve content, but got null stream in response.");
        }
        assert output != null : "Failed to initialize output stream.";
        long totalBytesWritten = 0;
        try {
            byte[] buffer = new byte[_bean.getBufferSize()];
            int read;
            while ((read = content.read(buffer)) != -1) {
                output.write(buffer, 0, read);
                totalBytesWritten += read;
            }
        } finally {
            output.flush();
        }
        return totalBytesWritten;
    }

    /**
     * Creates a new uniquely named file in the current working folder.
     *
     * @param extension The extension to append to the file.
     *
     * @return A {@link File} object representing the new uniquely named file.
     */
    private File generateUniqueFile(final String extension) {
        return generateUniqueFile(null, extension);
    }

    /**
     * Creates a new uniquely named file in the working folder.
     *
     * @param folder    The folder in which to put the file.
     * @param extension The suggested extension to append to the file. This can be overridden if explicitly set.
     *
     * @return A {@link File} object representing the new uniquely named file.
     */
    private File generateUniqueFile(final File folder, final String extension) {
        int index = -1;
        File local;
        final String pattern = getFilenamePattern(extension);
        String formatted = getGeneratedFileName(pattern, index);
        while ((local = folder == null ? new File(formatted) : new File(folder, formatted)).exists()) {
            index++;
            formatted = getGeneratedFileName(pattern, index);
        }
        return local;
    }

    private String getFilenamePattern(final String extension) {
        final String outputName = _bean.getOutputName();
        if (!StringUtils.isBlank(outputName)) {
            final String explicitExt = FilenameUtils.getExtension(outputName);
            boolean hasExtension = !StringUtils.isBlank(explicitExt);
            return (hasExtension ? FilenameUtils.removeExtension(outputName) : outputName) + "%s%s." + (hasExtension ? explicitExt : extension);
        } else {
            return String.format(GENERATED_FILE_NAME, "%s%s", extension);
        }
    }

    private String getGeneratedFileName(final String pattern, final int index) {
        return index == -1 ? String.format(pattern, "", "") : String.format(pattern, "-", StringUtils.leftPad(Integer.toString(index), 4, "0"));
    }

    private String getSuggestedExtension(final String contentType) {
        synchronized (EXTENSIONS_BY_MIME_TYPE) {
            if (EXTENSIONS_BY_MIME_TYPE.size() == 0) {
                BufferedReader reader = new BufferedReader(new InputStreamReader(getClass().getResourceAsStream("/META-INF/mime.types")));
                String line;
                try {
                    while ((line = reader.readLine()) != null) {
                        if (!line.trim().startsWith("#")) {
                            String[] atoms = line.trim().split("\\s+");
                            if (atoms.length > 1) {
                                EXTENSIONS_BY_MIME_TYPE.put(atoms[0], atoms[1]);
                            }
                        }
                    }
                } catch (IOException e) {
                    //
                }
            }
        }
        // If there's no content type specified, go with text since something will open that and you can figure it out from there.
        final String extension = EXTENSIONS_BY_MIME_TYPE.get(contentType);
        return StringUtils.isBlank(extension) ? EXTENSIONS_BY_MIME_TYPE.get("text/plain") : extension;
    }

    private boolean isTextContentType(final String contentType) {
        return getTextContentTypes().contains(contentType.contains(";") ? contentType.substring(0, contentType.indexOf(';')) : contentType);
    }

    private List<String> getTextContentTypes() {
        synchronized (TEXT_CONTENT_TYPES) {
            if (TEXT_CONTENT_TYPES.size() == 0) {
                TEXT_CONTENT_TYPES.addAll(Arrays.asList(getProperties().getProperty("contenttypes.text").split("\\s*,\\s*")));
            }
        }
        return TEXT_CONTENT_TYPES;
    }

    private void configureAuthentication() {
        if (StringUtils.isNotBlank(_bean.getUsername())) {
            final URI uri = _bean.getRemote();
            HttpHost targetHost = new HttpHost(uri.getHost(), uri.getPort(), uri.getScheme());
            _credentialsProvider.setCredentials(new AuthScope(targetHost.getHostName(), targetHost.getPort()), new UsernamePasswordCredentials(_bean.getUsername(), _bean.getPassword()));

            // Create AuthCache instance
            AuthCache authCache = new BasicAuthCache();

            // Generate BASIC scheme object and add it to the local auth cache
            BasicScheme basicAuth = new BasicScheme();
            authCache.put(targetHost, basicAuth);

            // Add AuthCache to the execution context
            _context.setCredentialsProvider(_credentialsProvider);
            _context.setAuthCache(authCache);
        } else if (StringUtils.isNotBlank(_bean.getSessionId())) {
            // Verify the JSESSIONID.
            for (Cookie cookie : _cookieStore.getCookies()) {
                if (cookie.getName().equals("JSESSIONID")) {
                    return;
                }
            }
            // If we don't have a JSESSIONID cookie, add it.
            final BasicClientCookie cookie = new BasicClientCookie("JSESSIONID", _bean.getSessionId());
            cookie.setDomain(_bean.getRemote().getHost());
            _cookieStore.addCookie(cookie);
        }
    }

    private HttpHost configureProxy() throws URISyntaxException {
        if (_bean.getProxy() == null) {
            return null;
        }

        final HttpHost proxy = new HttpHost(_bean.getProxy().getHost(), _bean.getProxy().getPort(), _bean.getProxy().getScheme());
        if (StringUtils.isNotBlank(_bean.getProxy().getUserInfo())) {
            final String[] credentials = _bean.getProxy().getUserInfo().split(":");
            _credentialsProvider.setCredentials(new AuthScope(_bean.getProxy().getHost(), _bean.getProxy().getPort()), new UsernamePasswordCredentials(credentials[0], credentials[1]));
        }
        return proxy;
    }

    private static class AllowAllCertsStrategy implements TrustStrategy {
        @Override
        public boolean isTrusted(X509Certificate[] chain, String authType) throws CertificateException {
            return true;
        }
    }

    private static final Log                  _log                    = LogFactory.getLog(XnatDataClient.class);
    private static final String               GENERATED_FILE_NAME     = "download%s.%s";
    private static final MimetypesFileTypeMap MIME_TYPE_MAP           = new MimetypesFileTypeMap();
    private static final List<String>         TEXT_CONTENT_TYPES      = new ArrayList<>();
    private static final Map<String, String>  EXTENSIONS_BY_MIME_TYPE = new HashMap<>();

    private final XnatDataClientPintoBean _bean;
    private final HttpClientBuilder       _clientBuilder;
    private final CookieStore             _cookieStore;
    private       CloseableHttpClient     _client;
    private final HttpClientContext       _context;
    private final CredentialsProvider _credentialsProvider = new BasicCredentialsProvider();
    private final PrintStream _printStream;
    private final Properties  _properties;
    private       String      _commonPath;
}
