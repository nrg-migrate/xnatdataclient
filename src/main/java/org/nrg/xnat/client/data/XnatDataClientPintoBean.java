/*
 * XnatDataClientPintoBean
 * (C) 2016 Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD License
 */
package org.nrg.xnat.client.data;

import com.google.common.base.Joiner;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nrg.framework.pinto.*;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class XnatDataClientPintoBean extends AbstractPintoBean {

    public XnatDataClientPintoBean(final Object parent, String[] arguments) throws PintoException {
        super(parent, arguments);
    }

    /**
     * Indicates the remote resource location. This should be a properly formatted URI resource identifier.
     *
     * @param remote The URI identifying the remote resource.
     */
    @Parameter(value = "r", longOption = "remote", help = "Indicates the remote resource location. This should be a properly formatted URL, although it can indicate file as well as http resources.", required = true)
    public void setRemote(URI remote) {
        _remote = remote;
    }

    /**
     * Returns the URI identifying the remote resource.
     *
     * @return The URI identifying the remote resource.
     */
    @Value("r")
    public URI getRemote() {
        if (_remote != null && !_allowUnsmoothUrls && !_urlHasBeenSmoothed) {
            _remote = smoothUrl(_remote);
            _urlHasBeenSmoothed = true;
            if (_log.isDebugEnabled()) {
                _log.debug("Smoothed remote URI to " + _remote);
            }
        }
        return _remote;
    }

    /**
     * Indicates the requested output name. This is the name to be used for the name of any output files. If the name is
     * already used in the output folder, XDC adds digits to the end of the file name prior to the extension. If no
     * extension is included on the output file name, XDC tries to use an appropriate extension based on the output
     * content type.
     *
     * @param outputName The requested output filename.
     */
    @Parameter(value = "o", longOption = "outputName", help = "Indicates the requested output name.")
    public void setOutputName(String outputName) {
        _outputName = outputName;
    }

    /**
     * Returns the requested output name.
     *
     * @return The requested output name.
     */
    @Value("o")
    public String getOutputName() {
        if (_outputName == null) {
            return "";
        }
        return _outputName;
    }

    /**
     * Indicates whether existing output files should be overwritten.
     *
     * @param overwrite Indicates whether output files should be overwritten.
     */
    @Parameter(value = "xx", longOption = "overwrite", argCount = ArgCount.StandAlone, help = "Indicates whether the specified output file should be overwritten without prompting if it exists.")
    public void setOverwrite(final boolean overwrite) {
        _overwrite = overwrite;
    }

    /**
     * Returns the specified overwrite mode.
     *
     * @return The specified overwrite mode.
     */
    @Value("xx")
    public boolean getOverwrite() {
        return _overwrite;
    }

    /**
     * The setter for the useAbsolutePath option.
     *
     * @param useAbsolutePath Incoming parameter.
     */
    @Parameter(value = "a", longOption = "useAbsolutePath", argCount = ArgCount.StandAlone, help = "Indicates that XDC should try to use the absolute path of specified resources for copy operations rather than REST calls.")
    public void setUseAbsolutePath(boolean useAbsolutePath) {
        if (_log.isDebugEnabled()) {
            _log.debug("Setting use absolute path to: " + useAbsolutePath);
        }
        _useAbsolutePath = useAbsolutePath;
    }

    /**
     * The getter for the useAbsolutePath option.
     *
     * @return Gets the value for the useAbsolutePath option.
     */
    @Value("a")
    public boolean getUseAbsolutePath() {
        if (_log.isDebugEnabled()) {
            _log.debug("Getting use absolute path: " + _useAbsolutePath);
        }
        return _useAbsolutePath;
    }

    /**
     * The setter for the showStatusCode option.
     *
     * @param showStatusCode Incoming parameter.
     */
    @Parameter(value = "c", longOption = "showStatusCode", argCount = ArgCount.StandAlone, help = "Tells XDC to show the resulting HTTP status code on exit.")
    public void setShowStatusCode(final boolean showStatusCode) {
        _showStatusCode = showStatusCode;
    }

    /**
     * The getter for the showStatusCode option.
     *
     * @return If <b>true</b>, indicates that the status code from the HTTP operation should be displayed on completion.
     */
    @Value("c")
    public boolean getShowStatusCode() {
        return _showStatusCode;
    }

    /**
     * The setter for the showStatusLine option.
     *
     * @param showStatusLine Incoming parameter.
     */
    @Parameter(value = "C", longOption = "showStatusLine", argCount = ArgCount.StandAlone, help = "Tells XDC to show the resulting HTTP status line on exit.")
    public void setShowStatusLine(final boolean showStatusLine) {
        _showStatusLine = showStatusLine;
    }

    /**
     * The getter for the showStatusLine option.
     *
     * @return If <b>true</b>, indicates that the status line from the HTTP operation should be displayed on completion.
     */
    @Value("c")
    public boolean getShowStatusLine() {
        return _showStatusLine;
    }

    /**
     * The setter for the pathPrefix option.
     *
     * @param pathPrefix Incoming parameter.
     */
    @Parameter(value = "pp", longOption = "pathPrefix", help = "Indicates a substitution for the path prefix. This lets you replace the first part of the returned absolute path with another path. The path tokens should be separated by the pound sign ('#'). For example, if you know that the absolute path returned by the server will be /data/project/archive/..., but your system has the same data archive mounted at /mnt/data/archive, you would specify the value for this option as /data/project#/mnt/data. Specifying this option implies the absolutePath option set to true.")
    public void setPathPrefix(String pathPrefix) {
        String[] paths = pathPrefix.split("#");
        if (paths.length != 2) {
            throw new RuntimeException("You must specify the path prefix option as two path prefixes separated by the pound sign ('#'). " + pathPrefix + " is an invalid value.");
        }
        _pathPrefixSource = paths[0];
        _pathPrefixDestination = paths[1];
    }

    /**
     * The getter for the pathPrefix option.
     *
     * @return Gets the value for the pathPrefix option.
     */
    @Value("pp")
    public String getPathPrefix() {
        if (StringUtils.isBlank(_pathPrefixSource) && StringUtils.isBlank(_pathPrefixDestination)) {
            return null;
        }
        return _pathPrefixSource + "#" + _pathPrefixDestination;
    }

    /**
     * The setter for the allow unsmooth URLs option.
     *
     * @param allowUnsmoothUrls Indicates that XDC should allow "unsmooth" URLs. "Unsmooth" URLs may have contiguous
     *                          path separators (i.e. forward slashes) and other artifacts from string arithmetic and
     *                          operations. By default, XDC smooths URLs before calling them.
     */
    @Parameter(value = "auu", longOption = "allowUnsmoothUrls", argCount = ArgCount.StandAlone, help = "Indicates that XDC should allow \"unsmooth\" URLs. \"Unsmooth\" URLs may have contiguous path separators (i.e. forward slashes) and other artifacts from string arithmetic and operations. By default, XDC smooths URLs before calling them (i.e. allow unsmooth URLs is false).")
    public void setAllowUnsmoothUrls(boolean allowUnsmoothUrls) {
        _allowUnsmoothUrls = allowUnsmoothUrls;
    }

    /**
     * The getter for the allowUnsmoothUrls option.
     *
     * @return Gets the value for the allowUnsmoothUrls option.
     */
    @Value("auu")
    public boolean getAllowUnsmoothUrls() {
        return _allowUnsmoothUrls;
    }

    /**
     * The setter for the batch option.
     *
     * @param batch Incoming parameter.
     */
    @Parameter(value = "b", longOption = "batch", argCount = ArgCount.StandAlone, help = "Indicates that this is a batch operation. Batch operations are currently only supported for download transfers, i.e. GET calls, that retrieve JSON that contains URI or absolutePath references.")
    public void setBatch(boolean batch) {
        _batch = batch;
    }

    /**
     * The getter for the batch option.
     *
     * @return Gets the value for the batch option.
     */
    @Value("b")
    public boolean getBatch() {
        return _batch;
    }

    /**
     * The setter for the useSymlinks option.
     *
     * @param useSymlinks Incoming parameter.
     */
    @Parameter(value = "k", longOption = "useSymlinks", argCount = ArgCount.StandAlone, help = "Indicates that files should be linked via symlinks rather that copied during batch operations. Note that this function is dependent on your platform supporting symlinks and the ln command being on your path.")
    public void setUseSymlinks(boolean useSymlinks) {
        _useSymlinks = useSymlinks;
    }

    /**
     * The getter for the useSymlinks option.
     *
     * @return Gets the value for the useSymlinks option.
     */
    @Value("k")
    public boolean getUseSymlinks() {
        return _useSymlinks;
    }

    /**
     * The setter for the listUris option.
     *
     * @param listUris Incoming parameter.
     */
    @Parameter(value = "ls", longOption = "listUris", argCount = ArgCount.StandAlone, help = "Directs the client to render results as a list of files to the application output. This can be used for such applications as piping output from a call to the server to other commands or processing servers.")
    public void setListUris(boolean listUris) {
        _listUris = listUris;
    }

    /**
     * The getter for the listUris option.
     *
     * @return Gets the value for the listUris option.
     */
    @Value("ls")
    public boolean getListUris() {
        return _listUris;
    }

    /**
     * Sets the HTTP method to be used to access the remote URI.
     *
     * @param method The HTTP method to be used. This can be one of GET, PUT, POST, or DELETE.
     *
     * @throws PintoException When an appropriate method is not specified.
     */
    @Parameter(value = "m", longOption = "method", help = "Indicates the HTTP method to be used for the operation. This can be one of GET, PUT, POST, or DELETE.")
    public void setMethod(String method) throws PintoException {
        if (StringUtils.isBlank(method) || !(method.equalsIgnoreCase("GET") || method.equalsIgnoreCase("PUT") || method.equalsIgnoreCase("POST") || method.equalsIgnoreCase("DELETE"))) {
            throw new PintoException(PintoExceptionType.SyntaxFormat, "You must specify one of GET, PUT, POST, or DELETE with the method parameter.");
        }
        _method = method;
    }

    /**
     * Gets the HTTP method to be used to access the remote URI.
     *
     * @return The HTTP method to be used.
     */
    @Value("m")
    public String getMethod() {
        return _method;
    }

    /**
     * Specifies an HTTP header to be added to the request. No validation is performed on the header name or value!
     *
     * @param headers The HTTP headers to be added to the request, expressed as a list of strings in the form
     *                "header=value".
     */
    @Parameter(value = "H", longOption = "header", argCount = ArgCount.OneToN, multiplesAllowed = true, help = "Specifies an HTTP header to be added to the request. The HTTP header should be expressed as a string in the form \"header=value\" or \"Header: Value\".")
    public void setHeaders(List<String> headers) throws PintoException {
        getHeaders().putAll(addKeyValueListToParameter(Collections.singletonList(Joiner.on("").join(headers)), "\\s*=\\s*|\\s*:\\s*"));
    }

    /**
     * Gets the HTTP method to be used to access the remote URI.
     *
     * @return The HTTP method to be used.
     */
    @Value("H")
    public Map<String, String> getHeaders() {
        if (_headers == null) {
            _headers = new HashMap<>();
        }
        return _headers;
    }

    /**
     * Convenience method to test whether any headers were added.
     *
     * @return <b>true</b> if any headers were specified on the command line, <b>false</b> otherwise.
     */
    public boolean hasHeaders() {
        return getHeaders().size() > 0;
    }

    /**
     * Indicates the local file to be uploaded.
     *
     * @param local Indicates the local file to be uploaded.
     */
    @Parameter(value = "l", longOption = "local", help = "Indicates the local file to be uploaded.")
    public void setLocal(File local) {
        _local = local;
    }

    /**
     * Indicates the local file to be uploaded.
     *
     * @return A local file.
     */
    @Value("l")
    public File getLocal() {
        return _local;
    }

    /**
     * The setter for the username option.
     *
     * @param username Incoming parameter.
     */
    @Parameter(value = "u", longOption = "username", help = "The user name for authentication.")
    public void setUsername(String username) {
        _username = username;
    }

    /**
     * The getter for the username option.
     *
     * @return Gets the value for the username option.
     */
    @Value("u")
    public String getUsername() {
        return _username;
    }

    /**
     * The setter for the password option.
     *
     * @param password Incoming parameter.
     */
    @Parameter(value = "p", longOption = "password", help = "The password for authentication.")
    public void setPassword(String password) {
        _password = password;
    }

    /**
     * The getter for the password option.
     *
     * @return Gets the value for the password option.
     */
    @Value("p")
    public String getPassword() {
        return _password;
    }

    /**
     * The setter for the session ID option.
     *
     * @param sessionId Incoming parameter.
     */
    @Parameter(value = "s", longOption = "sessionId", help = "Indicates the session ID to be used for transactions. This replaces username/password authentication options.")
    public void setSessionId(String sessionId) {
        _sessionId = sessionId;
    }

    /**
     * The getter for the session ID option.
     *
     * @return Gets the value for the session ID option.
     */
    @Value("s")
    public String getSessionId() {
        return _sessionId;
    }

    /**
     * The setter for the trustStore option.
     *
     * @param trustStore Incoming parameter.
     */
    @Parameter(value = "ts", longOption = "trustStore", help = "Indicates the location of the trust store.")
    public void setTrustStore(String trustStore) {
        _trustStore = trustStore;
    }

    /**
     * The getter for the trustStore option.
     *
     * @return Gets the value for the trustStore option.
     */
    @Value("ts")
    public String getTrustStore() {
        return _trustStore;
    }

    /**
     * The setter for the trustStorePassword option.
     *
     * @param trustStorePassword Incoming parameter.
     */
    @Parameter(value = "tsPass", longOption = "trustStorePassword", help = "Provides the password for accessing the trust store.")
    public void setTrustStorePassword(String trustStorePassword) {
        _trustStorePassword = trustStorePassword;
    }

    /**
     * The getter for the trustStorePassword option.
     *
     * @return Gets the value for the trustStorePassword option.
     */
    @Value("tsPass")
    public String getTrustStorePassword() {
        return _trustStorePassword;
    }

    /**
     * The setter for the proxy option. You can specify the proxy port, username and password, as required, as part of
     * the URI. For example:
     * <p/>
     * <div style='font-family: "Courier New", Courier, monospace'>http://<i>username</i>:<i>password</i>@<i>server</i>:<i>port</i>/path</div>
     *
     * @param proxy Incoming parameter.
     */
    @Parameter(value = "x", longOption = "proxy", help = "Indicates the server address for the proxy (if required).")
    public void setProxy(URI proxy) {
        _proxy = proxy;
    }

    /**
     * The getter for the proxy option.
     *
     * @return Gets the value for the proxy option.
     */
    @Value("x")
    public URI getProxy() {
        return _proxy;
    }

    /**
     * The setter for the bufferSize option.
     *
     * @param bufferSize Incoming parameter.
     */
    @Parameter(value = "bs", longOption = "bufferSize", help = "Sets the buffer size option. Defaults to 256.")
    public void setBufferSize(int bufferSize) {
        _bufferSize = bufferSize;
    }

    /**
     * The getter for the bufferSize option.
     *
     * @return Gets the value for the bufferSize option.
     */
    @Value("bs")
    public int getBufferSize() {
        return _bufferSize;
    }

    /**
     * The setter for the dataAscii option.
     *
     * @param data Incoming parameter.
     */
    @Parameter(value = "da", longOption = "data-ascii", help = "See -d, --data.")
    public void setDataAscii(String data) throws PintoException {
        if (data.startsWith("@")) {
            this._data = readFile(data.substring(1), true);
        } else {
            this._data = data;
        }
    }

    /**
     * The getter for the dataAscii option.
     *
     * @return Gets the value for the dataAscii option.
     */
    @Value("da")
    public String getDataAscii() {
        return _data;
    }

    /**
     * The setter for the data option.
     *
     * @param data Incoming parameter.
     */
    @Parameter(value = "d", longOption = "data", help = "Indicates data to be POSTed. If the data starts with an @, the rest of the data should be a file name from which to read data; carriage returns and newlines will be stripped out. This is identical to -da, --data-ascii.")
    public void setData(String data) throws PintoException {
        if (data.startsWith("@")) {
            this._data = readFile(data.substring(1), true);
        } else {
            this._data = data;
        }
    }

    /**
     * The getter for the data option.
     *
     * @return Gets the value for the dataAscii option.
     */
    @Value("d")
    public String getData() {
        return _data;
    }

    /**
     * The setter for the dataBinary option.
     *
     * @param dataBinary Incoming parameter.
     */
    @Parameter(value = "db", longOption = "data-binary", help = "Indicates data to be POSTed with no extra processing. If the data starts with an @, the rest of the data should be a file name from which to read data; carriage returns and newlines are preserved.")
    public void setDataBinary(String dataBinary) throws PintoException {
        if (dataBinary.startsWith("@")) {
            this._dataBinary = readFile(dataBinary.substring(1), false);
        } else {
            this._dataBinary = dataBinary;
        }
    }

    /**
     * The getter for the dataBinary option.
     *
     * @return Gets the value for the dataBinary option.
     */
    @Value("db")
    public String getDataBinary() {
        return _dataBinary;
    }

    /**
     * The setter for the accept self-signed certs option.
     *
     * @param acceptSelfSignedCerts Incoming parameter.
     */
    @Parameter(value = "z", longOption = "accept-self-signed-certs", help = "Indicates that the client should allow SSL connections to servers using self-signed SSL certificates. If you need to connect to a server with an invalid certificate (e.g. because the certificate has expired, consider the -zz option.")
    public void setAcceptSelfSigned(boolean acceptSelfSignedCerts) {
        _acceptSelfSignedCerts = acceptSelfSignedCerts;
    }

    @Value("z")
    public boolean getAcceptSelfSigned() {
        return _acceptSelfSignedCerts;
    }

    /**
     * The setter for the ignore cert errors option.
     *
     * @param ignoreCertErrors Incoming parameter.
     */
    @Parameter(value = "zz", longOption = "ignore-cert-errors", help = "Indicates that the client should allow SSL connections to servers with SSL certificates that are invalid or have errors, e.g. because the certificate has expired. This option should be used with caution and only to connect to servers that you trust and understand the cause of the underlying cert error.")
    public void setIgnoreCertErrors(boolean ignoreCertErrors) {
        _ignoreCertErrors = ignoreCertErrors;
    }

    @Value("zz")
    public boolean getIgnoreCertErrors() {
        return _ignoreCertErrors;
    }

    /**
     * A convenience method for determining whether a path prefix mapping was set.
     *
     * @return <b>true</b> if a path prefix was specified.
     */
    public boolean hasPathPrefix() {
        return _pathPrefixSource != null;
    }

    /**
     * Gets the parsed setting for the data source path prefix.
     *
     * @return The path prefix for the data source.
     */
    public String getPathPrefixSource() {
        return _pathPrefixSource;
    }

    /**
     * Gets the parsed setting for the data destination path prefix.
     *
     * @return The path prefix for the data destination.
     */
    public String getPathPrefixDestination() {
        return _pathPrefixDestination;
    }

    /**
     * Validate the processed parameters and their arguments.
     *
     * @throws PintoException When invalid parameters are found.
     */
    @Override
    public void validate() throws PintoException {
        if (noArgsHelp()) {
            return;
        }

        final boolean hasUsername = !StringUtils.isBlank(getUsername());
        final boolean hasPassword = !StringUtils.isBlank(getPassword());
        final boolean hasSessionId = !StringUtils.isBlank(getSessionId());
        if (hasUsername && !hasPassword || !hasUsername && hasPassword) {
            if (hasSessionId) {
                final String message = "You specified the " + (hasUsername ? "username" : "password") + " parameter as well as the session ID parameter. If you specify username or password, you must specify the other of the username or password. If you specify the session ID, you should specify the session ID ONLY.";
                throw new PintoException(PintoExceptionType.SyntaxFormat, message);
            } else {
                final String message = "You specified the " + (hasUsername ? "username" : "password") + " parameter, but not the corresponding " + (!hasUsername ? "username" : "password") + " parameter.";
                throw new PintoException(PintoExceptionType.SyntaxFormat, message);
            }
        }
        if (hasUsername && hasSessionId) {
            final String message = "You specified the username, password, and session ID parameters. You must specify either the username and password parameters or the session ID parameter ONLY.";
            throw new PintoException(PintoExceptionType.SyntaxFormat, message);
        }
        if (getRemote() == null) {
            if (getTrailingArguments().size() == 1) {
                try {
                    setRemote(new URI(getTrailingArguments().get(0)));
                } catch (URISyntaxException e) {
                    throw new PintoException(PintoExceptionType.SyntaxFormat, "The parameter is not a valid URL: " + getTrailingArguments().get(0));
                }
            } else {
                throw new PintoException(PintoExceptionType.SyntaxFormat, "You must specify the remote URI with which to access resources.");
            }
        }

        final String scheme = getRemote().getScheme();
        if (StringUtils.isBlank(scheme) || !(scheme.startsWith("http") || scheme.startsWith("https"))) {
            throw new PintoException(PintoExceptionType.SyntaxFormat, "XDC only supports the http or https protocols..");
        }
        if (StringUtils.isBlank(getMethod())) {
            setMethod("GET");
        }
        // This is just setting things properly. Specifying the path prefix substitution implies
        // the useAbsolutePath setting is true, so if it's not we'll set it that way.
        if (!getUseAbsolutePath() && !StringUtils.isBlank(getPathPrefix())) {
            setUseAbsolutePath(true);
        }
        if ((getListUris() || getUseSymlinks()) && !getBatch()) {
            setBatch(true);
        }
        if (getUseAbsolutePath() && !"GET".equalsIgnoreCase(getMethod())) {
            throw new PintoException(PintoExceptionType.SyntaxFormat, "You can only use the absolute path copy option with the GET HTTP method.");
        }
        if (getBatch() && !"GET".equalsIgnoreCase(getMethod())) {
            throw new PintoException(PintoExceptionType.SyntaxFormat, "You can only use the batch download option with the GET HTTP method.");
        }
        if (StringUtils.isNotBlank(getData()) && StringUtils.isNotBlank(getDataBinary())) {
            throw new PintoException(PintoExceptionType.SyntaxFormat, "You can't specify both textual and binary data in a single operation.");
        }
        if (getBufferSize() == 0) {
            setBufferSize(DEFAULT_BUFFER_SIZE);
        }
    }

    private String readFile(String fileName, boolean removeNewlines) throws PintoException {
        String fileContents;
        try {
            File file = new File(fileName);
            if (!file.exists()) {
                throw new PintoException(PintoExceptionType.SyntaxFormat, "File not found: " + fileName);
            }
            byte[] fileBytes = Files.readAllBytes(file.toPath());
            fileContents = new String(fileBytes);
        } catch (IOException e) {
            throw new PintoException(PintoExceptionType.SyntaxFormat, "File could not be read: " + fileName);
        }
        if (removeNewlines) {
            fileContents = fileContents.replace("\n", "");
            fileContents = fileContents.replace("\r", "");
        }
        return fileContents;
    }

    private URI smoothUrl(final URI uri) {
        if (uri == null) {
            throw new RuntimeException("You are trying to smooth a null URI.");
        }
        try {
            // Smoothing is really only concerned with the path.
            return !uri.getPath().contains("//") ? uri : new URI(uri.getScheme(), uri.getUserInfo(), uri.getHost(), uri.getPort(), uri.getPath().replaceAll("[/]{2,}", "/"), uri.getQuery(), uri.getFragment());
        } catch (URISyntaxException e) {
            // If for some reason creating a URI from what should be valid pieces fails, then just return the original
            // URI and hope for the best. That is at least a non-null URI.
            return uri;
        }
    }

    private static final Log _log                = LogFactory.getLog(XnatDataClientPintoBean.class);
    private static final int DEFAULT_BUFFER_SIZE = 256;

    private URI                 _remote;
    private String              _outputName;
    private boolean             _overwrite;
    private boolean             _useAbsolutePath;
    private boolean             _showStatusCode;
    private boolean             _showStatusLine;
    private String              _pathPrefixSource;
    private String              _pathPrefixDestination;
    private boolean             _allowUnsmoothUrls;
    private boolean             _urlHasBeenSmoothed;
    private boolean             _batch;
    private boolean             _useSymlinks;
    private boolean             _listUris;
    private Map<String, String> _headers;
    private String              _method;
    private File                _local;
    private String              _username;
    private String              _password;
    private String              _sessionId;
    private String              _trustStore;
    private String              _trustStorePassword;
    private URI                 _proxy;
    private int                 _bufferSize;
    private String              _data;
    private String              _dataBinary;
    private boolean             _acceptSelfSignedCerts;
    private boolean             _ignoreCertErrors;
}
