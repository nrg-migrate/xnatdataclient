/**
 * TestXnatDataClient
 * (C) 2014 Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD License
 *
 * Created on 10/31/2014 by Rick Herrick
 */
package org.nrg.xnat.client.data;

import com.github.tomakehurst.wiremock.junit.WireMockRule;
import org.junit.Rule;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static org.junit.Assert.assertEquals;

/**
 * TestXnatDataClient class.
 *
 * @author Rick Herrick
 */
// The WireMock stuff is documented here: http://wiremock.org/getting-started.html
public class TestXnatDataClient {
    private static final Logger _log = LoggerFactory.getLogger(TestXnatDataClient.class);

    @Rule
    public WireMockRule _wireMockRule = new WireMockRule(8089);

    @Test
    public void testSimpleGet() throws Exception {
        stubFor(get(urlEqualTo("/my/resource"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "text/xml")
                        .withBody("<response>Some content</response>")));
        XnatDataClient xdc = new XnatDataClient("http://localhost:8089/my/resource");
        int status = xdc.launch();
        assertEquals(0, status);
    }
}
